async function findUser(username, room) {
  try {
      const userExists = await strapi.services.users.find({ username, room });
      return userExists;
  } catch(err) {
      console.log("error while fetching", err);
  }
}
async function createUser({ username, room, status, socketId }) {
  try {
      const user = await strapi.services.users.create({
          username,
          room,
          status: status,
          socketId
      });
      return user;
  } catch(err) {
      console.log(err, "User couldn't be created. Try again!")
  }
}
async function userExists(id) {
  try {
      const user = await strapi.services.users.findOne({ id: id });
      return user;
  } catch(err) {
      console.log("Error occured when fetching user", err);
  }
}
async function getUsersInRoom(room) {
  try {
      const usersInRoom = await strapi.services.users.find({ room })
      return usersInRoom;
  } catch(err) {
      console.log("Error.Try again!", err);
  }
}

async function deleteUser(socketId) {
  try {
      const user = await strapi.services.users.delete({ socketId: socketId });
      return user;
  } catch(err) {
      console.log("Error while deleting the User", err);
  }
}

async function createRoom({roomName, chat}){
  try {
    const room = await strapi.services.chats.create({
      room : roomName,
      message : chat
    })
    return room
  } catch (error) {
    console.log(error, "Room couldn't be created. Try again!")
  }
}

async function findRoom(roomName) {
  try {
      const roomExists = await strapi.services.chats.find({ room: roomName});
      return roomExists;
  } catch(err) {
      console.log("error while fetching", err);
  }
}

async function roomExist(roomName) {
  try {
      const room = await strapi.services.chats.findOne({ room: roomName });
      return room;
  } catch(err) {
      console.log("Error occured when fetching room", err);
  }
}

async function updateRoom(roomName, chatArray){
  try {
    const room = await strapi.services.chats.update({room : roomName}, {message : chatArray}, {
      new : true
    })
    return room
  } catch (error) {
    console.log("Error on update chatroom", error);
  }
}

module.exports = {
  findUser,
  createUser,
  userExists,
  getUsersInRoom,
  deleteUser,
  createRoom,
  findRoom,
  roomExist,
  updateRoom
}