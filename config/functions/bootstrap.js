'use strict';
const {
  findUser, 
  createUser,
  userExists,
  getUsersInRoom,
  deleteUser,
  createRoom,
  findRoom,
  roomExist,
  updateRoom
} = require('./utils/database');

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#bootstrap
 */

module.exports = () => {
  var io = require('socket.io')(strapi.server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"],
      allowedHeaders: ["my-custom-header", "content-type"],
      credentials: true
    }
});

  io.on('connection', function(socket) {
    socket.on('join', async ({ username, room }, callback) => {
      console.log("user connected");
      console.log("username is ", username);
      console.log("room is...", room)
      console.log(socket.id, "socket.id")
      try {
        const userExists = await findUser(username, room);

        if(userExists.length > 0) {
          callback(`User ${username} already exists in room no${room}. Please select a different name or room`);
        } else {
          const user = await createUser({
            username: username,
            room: room,
            status: "ONLINE",
            socketId: socket.id
          });

          if(user) {
            const roomExisting = await findRoom(user.room);
            let arrayChat = []
            if(roomExisting.length > 0){
              // room exist and load message
              // let chatRoom = await 
              let roomAvalible = await roomExist(user.room)
              arrayChat = JSON.parse(roomAvalible.message)
            } else {
              // create room
              let room = await createRoom({
                roomName : user.room,
                chat : JSON.stringify(arrayChat)
              })
            }
            // arrayChat.push({
            //   user: 'bot',
            //   text: `${user.username}, Welcome to room ${user.room}.`,
            //   userData: user
            // })
          socket.join(user.room);
          socket.emit('welcome', {
            user: 'bot',
            text: `${user.username}, Welcome to room ${user.room}! \nStart Chat now to load all chat existing before (if avalible)`,
            userData: user
          });
          arrayChat.push({
            user: 'bot',
            text: `${user.username} has joined`,
          })
          socket.broadcast.to(user.room).emit('message', arrayChat);
          await updateRoom(user.room, JSON.stringify(arrayChat))

        } else {
          callback(`user could not be created. Try again!`)
        }
      }
      callback();
    } catch(err) {
      console.log("Err occured, Try again!", err);
    }
  });
  socket.on('sendMessage', async(data, callback) => {
    try {
      const user = await userExists(data.userId);
        if(user) {
          const roomAvalible = await roomExist(user.room);
          let arrayChat = []
          if(roomAvalible) {
            arrayChat = JSON.parse(roomAvalible.message)
            arrayChat.push({
              user: user.username,
              text: data.message,
            })
            io.to(user.room).emit('message', arrayChat);
            await updateRoom(user.room, JSON.stringify(arrayChat))
          } else {
            callback(`Room doesn't exist in the database. Rejoin the chat`)
          }
          // io.to(user.room).emit('message', [{
          //   user: user.username,
          //   text: data.message,
          // }]);
          // io.to(user.room).emit('roomInfo', {
          //   room: user.room,
          //   users: await getUsersInRoom(user.room)
          // });
        } else {
          callback(`User doesn't exist in the database. Rejoin the chat`)
        }
        callback();
      } catch(err) {
        console.log("err inside catch block", err);
      }
    });
    socket.on('disconnect', async(data) => {
      try {
        console.log("DISCONNECTED!!!!!!!!!!!!");
        const user = await deleteUser(socket.id);
        console.log("deleted user is", user)
        if(user.length > 0) {
          const roomAvalible = await roomExist(user[0].room);
          let arrayChat = []
          if(roomAvalible) {
            arrayChat = JSON.parse(roomAvalible.message)
            arrayChat.push({
              user: user[0].username,
              text: `User ${user[0].username} has left the chat.`,
            })
          }
          io.to(user[0].room).emit('message', arrayChat);  
          // io.to(user.room).emit('roomInfo', {
          //     room: user.room,
          //     users: await getUsersInRoom(user[0].room)
          // });
        }
      } catch(err) {
        console.log("error while disconnecting", err);
      }
    });
  });
};
